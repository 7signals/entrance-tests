# Typescript developer entry test

Hi and welcome.

This test is made to see on what level you are with React, Typescript and Javascript.
There are two distinct parts.

## Part ONE

You are going to be building a simple todo list with checkboxes to mark tasks as completed.

Create an ordered list of tasks with checkboxes in `packages/web-app` which will be used to keep track of the tasks done in part 2.

Tasks are available in `tasks.tsx` file.

To start the web-app run `yarn start:app` from the root.

**Part 1** is considered to be completed when we have following:

- list of tasks seen in the browser from 1...n
- checkboxes in front of the task description (respect the default `done` key in the task)
- being able to mark the task as `done`
- OPTIONAL!!! use `localStorage` for persistance HINT:: check the `helpers.tsx`

Layout is not important, what matters is to write the correct code with correct types.

## Part TWO

Check the list of the tasks that you have done in the Part 1.

To start the cli run `yarn start:cli` from the root.

**Operations have the same incoming and return signatures (types).**

**Part 2** is considered to be completed when you have executed the operations and console.logged the outputs. If you have done the tasks as you should, the output must match the solution that I made. That will be revealed after you finish this part.

The structure of the project is nothing that you haven't seen, so do what needs to be done.

Good luck, 🙌
