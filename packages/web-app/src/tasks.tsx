export interface Task {
  id: string
  done: boolean
  desc: string
}
const tasks: Task[] = [
  {
    id: 'uAKuN-_gxoB3ii-ZQMuHP',
    done: false,
    desc:
      'Fix the code in the packages/cli/src/main.ts file for the yarn start:cli to pass'
  },
  {
    id: 'FZVojfF9eHb5zDdD2uhFJ',
    done: false,
    desc:
      'Rename all Javascript files inside the package/cli/src/operations to Typescript'
  },
  {
    id: 'FZVojfF9eHb5zDdD2ssada',
    done: false,
    desc: 'Rewrite the JSDoc types to real Tyepscript types'
  },
  {
    id: 'qPUgX3FBarKoa1kl37fWp',
    done: false,
    desc:
      'Import all operations in packages/cli/src/main.ts file with one line without destructuring'
  },
  {
    id: 'vcPafU9XBbZgrRcxMpxtl',
    done: false,
    desc:
      'Open the photo under assets/joe-windsurfing.jpg using the nodejs filesystem module'
  },
  {
    id: 'kjEOYpnz3b0h805mbCzjt',
    done: true,
    desc:
      'NOTE:: Executed operations must have following signature  {genericHash: outputValue }[]'
  },
  {
    id: '7mOcVf8MSSQiXc93b8Mtq',
    done: false,
    desc: 'Execute all the operations with correct params'
  },
  {
    id: 'fx3vfiWHNmou8rco4Bn7-',
    done: false,
    desc: 'Set param `data` to be name of the file with extension'
  },
  {
    id: 'V4q6fZMrl0OysWgAxkWki',
    done: false,
    desc: 'console.log all the executed operations with their output values'
  },
  {
    id: 'gwVjTukLsyc0R6TAdL8eP',
    done: false,
    desc:
      'OPTIONAL!!! console log the names of the operations from the import using string literals'
  }
]

export default tasks
