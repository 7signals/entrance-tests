import { useEffect, useState } from 'react'
import { Task } from './tasks'

/**
 * useState dropin replacement with localstorage
 * STOLEN and modified from https://joshwcomeau.com/react/persisting-react-state-in-localstorage/
 * @param tasks
 * @param key
 */
export function useStickyState (
  tasks: Task[],
  key: string
): [Task[], React.Dispatch<any>] {
  const [value, setValue] = useState(() => {
    const stickyValue = window.localStorage.getItem(key)
    return stickyValue !== null ? JSON.parse(stickyValue) : tasks
  })
  useEffect(() => {
    window.localStorage.setItem(key, JSON.stringify(value))
  }, [key, value])
  return [value, setValue]
}
