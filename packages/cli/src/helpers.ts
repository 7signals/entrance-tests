import CID from 'cids'
import * as imghash from 'imghash'
import mh from 'multihashing-async'

/**
 * Calculate the Multihash
 */
export async function calculateHash (
  data: Buffer,
  algo = 'blake2b',
  length = 256
): Promise<Buffer> {
  const hash = await mh(data, `${algo}-${length}`)
  return hash
}
/**
 * Create CID and return the instance
 */
export function createCID (
  data: Buffer,
  codec = 'raw',
  baseName = 'base32'
): CID {
  return new CID(1, codec, data, baseName)
}

/**
 * Use only binary ascii representation
 */
export async function calculatePhash (b: Buffer): Promise<string> {
  return imghash.hash(b, 16, 'binary')
}

/**
 * Original Document ID sometimes is a UUID without hyphens, this makes it look nice
 * later maybe add the is-uuid check https://runkit.com/woss/is-uuid-guid
 * @param str {string}
 * @return  {string}
 */
export function formatToUUID (uuid: string): string {
  // return the parse one, no check here

  if (uuid.split('-').length === 5) {
    return uuid
  }

  const r = new RegExp(
    /([A-Za-z0-9]{8})([A-Za-z0-9]{4})([A-Za-z0-9]{4})([A-Za-z0-9]{4})([A-Za-z0-9]{12})/gi
  )

  const matches = r.exec(uuid)
  if (matches.length !== 6) {
    throw new Error('Invalid UUID')
  }

  const ret = matches
    .slice(1, matches.length)
    .join('-')
    .toLowerCase()

  return ret
}
