/**
 * Operation specification
 */
export default {
  id: 'bafk2bzacebjktneyfak7elakh5jekmd4gapilx3wzxedbg3t7uef2en6wxsjg',
  data: {
    op: 'generic_hash',
    desc: 'Generic blake2b 256 hashing op. Any kind of data',
    hashing: {
      algo: 'blake2b',
      bits: 256,
      skip: false
    },
    encoding: {
      algo: 'hex',
      prefix: true
    },
    ops: []
  }
}
