import { calculateHash, createCID } from '../../helpers'
import config from './config'

/**
 * Execute genericHash
 * @param {{file: Buffer, rawPixels: Buffer: data: Buffer}} params
 * @return string
 */
export default async function genericHash ({ data }) {
  let ret = ''
  ret = createCID(
    await calculateHash(
      data,
      config.data.hashing.algo,
      config.data.hashing.bits
    )
  ).toString()
  return ret
}
