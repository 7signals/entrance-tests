import ExifReader from 'exifreader'
import { calculateHash, createCID } from '../../helpers'
import { formatToUUID } from '../../proofs/execution'
import config from './config'

/**
 * Execute metaOriginalDocumentId
 * @param {{file: Buffer, rawPixels: Buffer}} params
 * @return string
 */
export default async function metaOriginalDocumentId ({ file, rawPixels }) {
  let ret = ''
  let val = ''
  const tags = ExifReader.load(file)
  const { OriginalDocumentID } = tags

  if (OriginalDocumentID && OriginalDocumentID.value) {
    // in most of my photos this is uuid but without the -
    val = formatToUUID(OriginalDocumentID.value)
  }

  ret = createCID(
    await calculateHash(
      Buffer.from(val),
      config.data.hashing.algo,
      config.data.hashing.bits
    )
  ).toString()
  return ret
}
