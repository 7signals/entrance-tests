/**
 * Operation specification
 */
export default {
  id: 'bafk2bzacecjkupobs3bpluxluzg3oleimzeplqbf7forkqqglnronnjaxbgik',
  data: {
    op: 'meta_original_document_id',
    desc:
      'Original Document ID. The common identifier for the original resource from which the.to_vec() current resource is derived. For example, if you save a resource to a different format, then save that one to another format, each save operationData should generate a new xmpMM:DocumentID that uniquely identifies the resource in that format, but should retain the ID of the source file here.',
    hashing: {
      algo: 'blake2b',
      bits: 256,
      skip: false
    },
    encoding: {
      algo: 'hex',
      prefix: true
    },
    ops: []
  }
}
