/**
 * Operation specification
 */
export default {
  id: 'bafk2bzaced2wspmdlikt43quhqlp5jwcozrzzxd23p3d5zcnbkdv35dyrjeka',
  data: {
    op: 'perceptual_hash',
    desc:
      'Perceptual hash calculation, currently implementing http://blockhash.io/',
    hashing: {
      algo: '',
      bits: 0,
      skip: true
    },
    encoding: {
      algo: 'hex',
      prefix: true
    },
    ops: []
  }
}
