import { calculatePhash } from '../../helpers'
import config from './config'

/**
 * Execute perceptualHash
 * @param {{file: Buffer, rawPixels: Buffer}} params
 * @return string
 */
export default async function perceptualHash ({ file }) {
  let ret = ''
  const phash = await calculatePhash(file)
  if (config.data.ops.length) {
    throw new Error(
      "ExecuteOps:: OP has more ops, we don't support that just yet."
    )
  }
  ret = phash
  return ret
}
