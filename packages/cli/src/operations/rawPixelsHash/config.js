/**
 * Operation specification
 */
export default {
  id: 'bafk2bzaceato2fbapmff54qgjgaba3v54qhz4g2ged54kebkio4hvowi6a3aa',
  data: {
    op: 'raw_pixels_hash',
    desc:
      'Metadata must be removed and has must be created off of the RAW PIXELS',
    hashing: {
      algo: 'blake2b',
      bits: 256,
      skip: false
    },
    encoding: {
      algo: 'hex',
      prefix: true
    },
    ops: []
  }
}
