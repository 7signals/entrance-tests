import { calculateHash, createCID } from '../../helpers'
import config from './config'

/**
 * Execute rawPixelsHash
 * @param {{file: Buffer, rawPixels: Buffer}} params
 * @return string
 */
export default async function rawPixelsHash ({ rawPixels }) {
  let ret = ''

  ret = createCID(
    await calculateHash(
      rawPixels,
      config.data.hashing.algo,
      config.data.hashing.bits
    )
  ).toString()
  return ret
}
